#include "Persona.h"
#include <string>


Persona::Persona()
{
  this->m_nombres = "";
  this->m_apellidos = "";
}
Persona::~Persona()
{
}
string Persona::getNombres()
{
  return m_nombres;
}
void Persona::setNombres(string nombres)
{
  this->m_nombres = nombres;
}
string Persona::getApellidos()
{
  return this->m_apellidos;
}
void Persona::setApellidos(string apellidos)
{
  this->m_apellidos = apellidos;
}
string Persona::getNombrecompleto()
{
  return this->m_nombres + ", " + this->m_apellidos;
}
