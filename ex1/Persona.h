#include <string>

using namespace std;

class Persona
{
  private:
    string m_nombres;
    string m_apellidos;
  public:
    Persona();
    ~Persona();
    string getNombres();
    void setNombres(string nombres);
    string getApellidos();
    void setApellidos(string apellidos);
    string getNombrecompleto();
};
