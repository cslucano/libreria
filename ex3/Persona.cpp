#include "Persona.h"
#include <string>


Persona::Persona()
{
  this->m_nombres = "";
  this->m_apellidos = "";
}
Persona::Persona(json_object* obj)
{
  this->m_nombres = json_object_get_string(json_object_object_get(obj, "Nombres"));
  this->m_apellidos = json_object_get_string(json_object_object_get(obj, "Apellidos"));
}
Persona::~Persona()
{
}
string Persona::getNombres()
{
  return m_nombres;
}
void Persona::setNombres(string nombres)
{
  this->m_nombres = nombres;
}
string Persona::getApellidos()
{
  return this->m_apellidos;
}
void Persona::setApellidos(string apellidos)
{
  this->m_apellidos = apellidos;
}
string Persona::toString()
{
  return this->m_apellidos;
}
string Persona::toJson()
{
  return json_object_to_json_string(getJson());
}

json_object* Persona::getJson()
{
  json_object* jPersona= json_object_new_object();
  json_object* jNombres = json_object_new_string(this->m_nombres.data());
  json_object* jApellidos = json_object_new_string(this->m_apellidos.data());
  json_object_object_add(jPersona,"Nombres",jNombres);
  json_object_object_add(jPersona,"Apellidos",jApellidos);

  return jPersona;
}
