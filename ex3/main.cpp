#include <iostream>
#include "Libro.h"

int main()
{

  Persona* autor = new Persona();
  autor->setNombres("Jeffrey");
  autor->setApellidos("Ullman");

  Libro* l = new Libro();
  l->setISBN("ISBN-0-13-713526-2");
  l->setTitulo("A First Course in Database Systems");
  l->setPrecio(100);
  l->addAutor(autor);

  cout<<"Libro: "<<l->toJson().data()<<endl;
  return 0;
}
