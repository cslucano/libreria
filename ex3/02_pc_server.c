// compilar con gcc -pthread pc.c -o pc
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

#define BUF_TAM 50
#define NUM_PROD 2
#define NUM_CONS 2

#define SOCK_PATH "prodcons_socket"

int contador;
int buffer[BUF_TAM];

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t semalleno;
sem_t semavacio;

void *productor();
void *consumidor();
void *print_buffer();


struct conn {
    FILE * s_in;
    FILE * s_out;
};

int inicializarservidor();
void *procesarcommando(void*);
struct conn * handler(int);


void *productor()
{
    int i=0;
    while(1)
    {
        sem_wait(&semavacio);
        pthread_mutex_lock(&mutex);
        if(contador<BUF_TAM)
        {
            buffer[contador] = i;
            contador = contador + 1;
            //printf("p > %d\n",contador);
        }
        pthread_mutex_unlock(&mutex);
        sem_post(&semalleno);
        usleep(10);
    }
}
void *consumidor()
{
    while(1)
    {
        sem_wait(&semalleno);
        pthread_mutex_lock(&mutex);
        if(contador>0)
        {
            contador = contador -1;
            //printf("c < %d\n",contador);
        }
        pthread_mutex_unlock(&mutex);
        sem_post(&semavacio);
        usleep(10);
    }
}

int main()
{
  contador = 0;
  sem_init(&semalleno,0,0);
  sem_init(&semavacio,0,BUF_TAM);
  pthread_t tid_c; 
  pthread_t tid_p;  
  pthread_t tid_print;  

  pthread_attr_t attr;
  pthread_attr_init(&attr);

  int i;
  for(i=0; i < NUM_PROD; i++) {
    pthread_create(&tid_p,&attr,productor,NULL);
  }
  for(i=0; i < NUM_CONS; i++) {
    pthread_create(&tid_c,&attr,consumidor,NULL);
  }

  //pthread_create(&tid_print,&attr,print_buffer,NULL);

  //pthread_join(tid_p, NULL);
  //pthread_join(tid_c, NULL);

  int sock_local;
  sock_local = inicializarservidor();

  while(1)
  {
    struct conn * c = handler(sock_local);
    pthread_t tid_worker; 
    pthread_create(&tid_worker, NULL, procesarcommando , (void *) c);
  }
  
  return 0;
}

void *print_buffer()
{
    while(1)
    {
        pthread_mutex_lock(&mutex);
        int i = 0;
        for(i = 0; i < contador; i++ )
        { 
            fprintf(stdout, "%d", buffer[contador]);
            fflush(stdout);
        }
        pthread_mutex_unlock(&mutex);
        usleep(100);
        //pthread_yield();
    }
}


int inicializarservidor()
{
    int s, len;
    struct sockaddr_un local;

    s = socket(AF_UNIX, SOCK_STREAM, 0);
    s = socket(AF_UNIX, SOCK_STREAM, 0);


    local.sun_family = AF_UNIX;
    strcpy(local.sun_path, SOCK_PATH);
    unlink(local.sun_path);
    len = strlen(local.sun_path) + sizeof(local.sun_family);

    if(bind(s, (struct sockaddr *)&local, len) == -1)
      printf("Error bind");

    listen(s, 5);

    return s;

}

struct conn * handler(int socket_local)
{
    printf("Esperando... \n");
    int conexion;
    if( (conexion = accept(socket_local, NULL, NULL)) < 0 )
    {
        printf("Error en la conexion");
        return 0;
    }
    else 
    {
        struct conn *s;
        s = malloc(sizeof(struct conn));
        s->s_in = fdopen(conexion, "r");
        s->s_out = fdopen(conexion, "w");
        setlinebuf(s->s_in);
        setlinebuf(s->s_out);
        return s;
    }
}

void *procesarcommando(void * c)
{
    struct conn * s = c;
    char buff[100];
    fprintf(s->s_out, "Ingrese commando:\n");
    while(fgets(buff, sizeof(buff), s->s_in) != 0)
    { 
        fprintf(s->s_out, "Ingrese commando:\n");
        printf("%s", buff) ;
        fflush(stdout) ;
        if (buff[0] == 'p') {
          int i;
          for(i=0; i < contador; i++)
            fprintf(s->s_out, "%d", buffer[i]);
          fprintf(s->s_out, "\n");
          fflush(s->s_out);
        }
        else if (buff[0] == 'c') {
          ///Trabajo de consumidor
        }
        ///Imprimir el buffer
    }
}
