#include <string>
#include <vector>
#include "Persona.h"

using namespace std;

class Libro
{
  private:
    string m_ISBN;
    string m_titulo;
    float m_precio;
    vector<Persona>* m_autores;
  public:
    Libro();
    Libro(json_object*);
    ~Libro();

    string getISBN();
    void setISBN(string ISBN);

    string getTitulo();
    void setTitulo(string titulo);

    float getPrecio();
    void setPrecio(float precio);

    vector<Persona>* getAutores();
    void addAutor(Persona* autor);

    string toString();
    string toJson();

    json_object* getJson();
};
