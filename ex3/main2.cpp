#include <iostream>
#include <json/json.h>
#include "Libro.h"

int main()
{
  char *archivo = "Libreria.json";
  json_object* libreria = json_object_from_file(archivo);

  json_object* libros = json_object_object_get(libreria, "Libros");

  for(int i=0; i < json_object_array_length(libros); i++)
  {
    cout<<json_object_to_json_string(json_object_array_get_idx(libros, i)) <<endl;
  }

  cout<< json_object_to_json_string(libreria) <<endl;
  return 0;
}
