// compilar con gcc -pthread pc.c -o pc
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

#define BUF_TAM 50
#define NUM_PROD 2
#define NUM_CONS 2

#define SOCK_PATH "prodcons_socket"

struct conn {
    FILE * s_in;
    FILE * s_out;
};

struct conn *  inicializarsocketremoto();
void *procesarcomando(void*);
void *proc_salida(void * c);


int main()
{

  struct conn * c = inicializarsocketremoto();
  pthread_t tid; 
  pthread_create(&tid, NULL, proc_salida , (void *) c);
  while(1)
    procesarcomando((void*)c);

  //while(1)
  //{
  //  struct conn * c = handler(sock_remoto);
  //  pthread_t tid_worker; 
  //  pthread_create(&tid_worker, NULL, procesarcomando , (void *) c);
  //}
  
  return 0;
}


struct conn * inicializarsocketremoto()
{
    int s, len;
    struct sockaddr_un remoto;

    s = socket(AF_UNIX, SOCK_STREAM, 0);

    remoto.sun_family = AF_UNIX;
    strcpy(remoto.sun_path, SOCK_PATH);
    len = strlen(remoto.sun_path) + sizeof(remoto.sun_family);

    if( connect(s, (struct sockaddr *)&remoto, len ) == -1)
      printf("Error al inicializar socket");
    
    struct conn *socket;
    socket = malloc(sizeof(struct conn));
    socket->s_in = fdopen(s, "r");
    socket->s_out = fdopen(s, "w");
    setlinebuf(socket->s_in);
    setlinebuf(socket->s_out);

    return socket;

}

void *procesarcomando(void * c)
{
    struct conn * s = c;
    char buff[100];
    //printf("Ingrese comando: \n");
    int t = 0;
    //if( gets(buff) != 0)
    if( scanf("%s", buff) )
    { 
        //buff[strlen(buff)] = '\0';
        //printf("%s", buff);
        fprintf(s->s_out, "%s\n", buff);
        //fflush(s->s_out);
    }
}

void *proc_salida(void * c)
{
  struct conn * s = c;
  char buff[100];
  while(fgets(buff,sizeof(buff),s->s_in) != 0)
  {
    printf("%s\n", buff);
  }
}
