#include "Libro.h"
#include <string>
#include <vector>
#include <json/json.h>

using namespace std;

Libro::Libro()
{
  this->m_ISBN = "";
  this->m_titulo = "";
  this->m_precio = 0.0;
  this->m_autores = new vector<Persona*>();
}
Libro::Libro(json_object* obj)
{
  this->m_ISBN = json_object_get_string(json_object_object_get(obj, "ISBN"));
  this->m_titulo = json_object_get_string(json_object_object_get(obj, "Titulo"));
  this->m_precio = json_object_get_double(json_object_object_get(obj, "Precio"));
  this->m_autores = new vector<Persona*>();
  json_object* jautores = json_object_object_get(obj, "Autores");
  for(int i=0; i < json_object_array_length(jautores) ; i++)
  {
    Persona* autor = new Persona(json_object_array_get_idx(jautores, i));
    this->m_autores->push_back(autor);
  }
}
Libro::~Libro()
{
  for(int i = 0; i < this->m_autores->size(); i++)
    delete (*this->m_autores)[i];
  delete this->m_autores;
}
string Libro::getISBN()
{
  return m_ISBN;
}
void Libro::setISBN(string ISBN)
{
  this->m_ISBN = ISBN;
}
string Libro::getTitulo()
{
  return this->m_titulo;
}
void Libro::setTitulo(string titulo)
{
  this->m_titulo = titulo;
}
float Libro::getPrecio()
{
  return this->m_precio;
}
void Libro::setPrecio(float precio)
{
  this->m_precio = precio;
}
vector<Persona*>* Libro::getAutores()
{
  return this->m_autores;
}
void Libro::addAutor(Persona* autor)
{
  this->m_autores->push_back(autor);
}
string Libro::toString()
{
  return this->m_titulo;
}
string Libro::toJson()
{
  return json_object_to_json_string(getJson());
}

json_object* Libro::getJson()
{
  json_object* jLibro= json_object_new_object();

  json_object* jISBN = json_object_new_string(this->m_ISBN.data());
  json_object* jTitulo = json_object_new_string(this->m_titulo.data());
  json_object* jPrecio = json_object_new_double(this->m_precio);
  json_object* jAutores = json_object_new_array();

  json_object_object_add(jLibro,"ISBN",jISBN);
  json_object_object_add(jLibro,"Titulo",jTitulo);
  json_object_object_add(jLibro,"Precio",jPrecio);
  json_object_object_add(jLibro,"Autores",jAutores);
  vector<Persona*>::iterator it;
  for(it = this->m_autores->begin(); it < this->m_autores->end() ; it++ )
    json_object_array_add(jAutores,(*it)->getJson());

  return jLibro;
}
