#include <iostream>
#include "Libro.h"

int main()
{


  Libro* libro = new Libro();
  libro->setISBN("ISBN-0-13-713526-2");
  libro->setTitulo("A First Course in Database Systems");
  libro->setPrecio(100);

  Persona* autor = new Persona();
  autor->setNombres("Jeffrey");
  autor->setApellidos("Ullman");
  libro->addAutor(autor);

  autor = new Persona();
  autor->setNombres("Jose");
  autor->setApellidos("Perez");
  libro->addAutor(autor);

  cout<<"Libro: "<<libro->toJson()<<endl;
  //cout<<"Libro: "<<libro->toString()<<endl;
  //cout<<"Libro: "<<autor3.toString()<<endl;
  
  delete libro;
  return 0;
}
