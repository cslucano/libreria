#include <iostream>
#include "Libro.h"

int main()
{

  char *archivo = "Libreria.json";
  json_object* libreria = json_object_from_file(archivo);

  json_object* libros = json_object_object_get(libreria, "Libros");

  Libro* l = new Libro(json_object_array_get_idx(libros, 1));

  //cout<<"Libro: "<<l->toJson().data()<<endl;
  cout<<"Libro: "<<l->toJson()<<endl;
  return 0;
}
