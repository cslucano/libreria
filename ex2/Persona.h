#include <string>
#include <json/json.h>

using namespace std;

class Persona
{
  private:
    string m_nombres;
    string m_apellidos;
  public:
    Persona();
    Persona(json_object*);
    ~Persona();
    string getNombres();
    void setNombres(string nombres);
    string getApellidos();
    void setApellidos(string apellidos);

    string toString();
    string toJson();

    json_object* getJson();
};
